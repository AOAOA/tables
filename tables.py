import os
import csv

import cells

class table:
    """Class for handling tables"""
    def __init__(self, table):
        self.matrix = []
        self.rows = []
        self.columns = []
        self.cells = 0
        self.path = ''
        self.names = dict()
        self.index = 0

        if str(type(table)) == "<class 'str'>":
            self.path = table
            table_file = open(table, 'r+', newline='')
        elif str(type(table)) == "<class '_io.TextIOWrapper'>":
            table_file = table
            self.path = os.path.realpath(table_file.name)

        for line in csv.reader(table_file, delimiter=';', quotechar='"'):
            self.matrix.append([cells.cell(x) for x in line])

        table_file.close()

        self.rows = [x for x in range(0, len(self.matrix))]
        self.columns = [x for x in self.matrix[0]]
        self.cells = len(self.rows) * len(self.columns)
        self.index = len(self.rows)


    def __str__(self):
        print_str = ''
        for row in self.matrix:
            for x in range(0, len(row)):
                if x != (len(row) - 1):
                    print_str += str(row[x]) + ', '
                else:
                    print_str += str(row[x])
            print_str += '\n'

        return print_str

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == 0:
            self.index = len(self.rows)
            raise StopIteration
        self.index -= 1
        return self.matrix[self.index]

    def area(self, address):
        """Returns a tuple of cells contained in an address"""
        addresses = address.split(':')
        if len(addresses) % 2 != 0 and len(addresses) != 1:
            raise InvalidRangeError
