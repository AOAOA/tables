import re

class cell:
    """Class that represents a single cell from a table"""
    __rgxes = {
            'secs_mins': r'[0-5]?[0-9]{1}', 'hour': r'^(2[0-3]{1}|[01]{1}[0-9]{1}):', 
            'day': r'^(3[01]|[0-2]?[0-9]{1})/', 'month': r'(1[012]{1}|[0]?[1-9]{1})', 
            'year': r'/([12]{1}[0-9]{3})$'
    }
    __valid_formats = {
            'datetime': (f'{__rgxes["day"]}{__rgxes["month"]}{__rgxes["year"]}', 
            f'{__rgxes["day"]}{__rgxes["month"]}', f'{__rgxes["hour"]}{__rgxes["secs_mins"]}:{__rgxes["secs_mins"]}', 
            f'{__rgxes["hour"]}{__rgxes["secs_mins"]}', f'{__rgxes["secs_mins"]}:{__rgxes["secs_mins"]}'), 
            'string': (r'([a-z]|[A-Z]|[0-9]){1,20}',), 
            'number': (r'test',)
    }

    def __init__(self, arg_value='', arg_format=''):
        self.__address = ''
        self.__value = ''
        self.__value_format = ''

        if arg_format == 'guess':
            self.__value_format = self.guessFormat(arg_value) 
        elif arg_format != '':
            self.setFormat(arg_format)

        if arg_value != '':
            self.setValue(arg_value)

    def guessFormat(self, arg_value):
        """Guesses the format of a value by trying to match it with the 
            defined formats"""
        for frmt_type in self.__valid_formats:
            for rgx in frmt_type:
                if re.fullmatch(rgx, str(arg_value)):
                    return True
        return False

    def checkValue(self, arg_value):
        """Checks if a value matches the defined format for the cell"""
        if self.__value_format == 'number': return True
        for rgx in self.__valid_formats[self.__value_format]:
            if re.fullmatch(rgx, arg_value):
                return True
        return False

    def setValue(self, arg_value):
        """Sets the cell value if it matches the defined format"""
        self.__value = arg_value

    def getValue(self):
        """Returns the formatted cell value"""
        return self.__value

    def setFormat(self, arg_format):
        """Sets the format of the cell"""
        if arg_format in self.__valid_formats:
            self.__value_format = arg_format

    def getFormat(self):
        """Returns the format of the cell"""
        return self.__value_format

    def __str__(self):
        return str(self.__value)

    def __repr__(self):
        return self.__str__()


def printo(testa):
    print(testa)


class InvalidRangeError(Exception):
    """Exception raised when a range of cells is not valid"""
    def __init__(self):
        pass
